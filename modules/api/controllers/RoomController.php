<?php

namespace app\modules\api\controllers;

use app\models\AutoRate;
use app\models\AutoRateLog;
use app\models\PortfolioRateAuto;
use app\models\Portfolio;
use app\models\Room;
use app\models\RoomSetting;
use app\models\RoomUser;
use app\models\Settings;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\User;
use app\models\PortfolioRate;

/**
 * Default controller for the `api` module
 */
class RoomController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionTest()
    {
        $apiKey = Settings::findByKey('wallet_token')->value;

        $ch = curl_init('https://cryptoprocessing.io/api/v1/wallets');

        $data = [
            "name" => 'crypto_btc',
            'currency' => "USDT",
            "human" => "USDT",
            'description' => 'test usdt wallet'
        ];

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Token {$apiKey}", "Content-Type: application/json", "Idempotency-Key: 40ijhb93b534"]);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        VarDumper::dump(json_decode($result, true), 10, true);
        VarDumper::dump($info, 10, true);

        // 5ff6842d-279f-49bc-985a-be346fc4fe65
    }

    public function actionTestAddress()
    {
        $apiKey = Settings::findByKey('wallet_token')->value;

        $ch = curl_init('https://cryptoprocessing.io/api/v1/wallets/5ff6842d-279f-49bc-985a-be346fc4fe65/addresses');


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Token {$apiKey}", "Content-Type: application/json"]);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        VarDumper::dump(json_decode($result, true), 10, true);
        VarDumper::dump($info, 10, true);

        //33TiAq8KFWNiQfgfMFZsz2wggxKUVgaP8s
    }

    public function actionSetWebhook()
    {
        $apiKey = Settings::findByKey('wallet_token')->value;

        $ch = curl_init('https://cryptoprocessing.io/api/v1/wallets/5ff6842d-279f-49bc-985a-be346fc4fe65/webhooks');

        $data = [
            "url" => Url::toRoute(['room/webhook'], true),
            'max_confirmations' => "6",
            "max_retries" => "2",
        ];

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Token {$apiKey}", "Content-Type: application/json"]);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        VarDumper::dump(json_decode($result, true), 10, true);
        VarDumper::dump($info, 10, true);
    }

    public function actionWebhook()
    {

    }

    /**
     *
     */
    public function actionAwait()
    {
        $models = Room::find()->where(['status' => Room::STATUS_ACCEPT])->all();

        foreach ($models as $model)
        {
            if(date('Y-m-d H:i') >= date('Y-m-d H:i', strtotime($model->datetime_accept_end))){
                $model->status = Room::STATUS_AWAIT;
                $model->save(false);
            }
        }
    }

 /**
     *
     */
    public function actionEnd2()
    {
        $models = Room::find()->where(['id' => 13190])->all();


//        echo $a;
//        exit;

        foreach ($models as $model)
        {
                $result = 1;


//                $result = rand(0, 1);

                $roomUsers = RoomUser::find()->where(['room_id' => $model->id])->all();

                $sumWin = RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => $result])->sum('rate');
                $sumWinCount = RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => $result])->count();
                $sumFail = RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => ($result == 1 ? 0 : 1)])->sum('rate');

                $resultCommission = ($sumFail / 100) * $model->service_commission;

                $sumFail = $sumFail - $resultCommission;

                VarDumper::dump($resultCommission);

//                $sumWin = $sumWin / $sumWinCount;

//                $model->service_commission = $resultCommission;

                $model->status = Room::STATUS_END;
//                $model->save(false);

                $setting = RoomSetting::findOne($model->room_setting_id);

                $newRoom = new Room([
                    'name' => $setting->name,
                    'status' => Room::STATUS_ACCEPT,
                    'room_setting_id' => $setting->id,
                    'service_commission' => $setting->commission,
                    'diff_rate_one' => $setting->diff_rate_one,
                    'diff_rate_two' => $setting->diff_rate_two,
                    'datetime_accept_end' => date('Y-m-d H:i:s', time() + $setting->time_accept * 60),
                    'datetime_end' => date('Y-m-d H:i:s', time() + $setting->time_await * 60),
                    'min_value' => $setting->min_value,
                    'max_value' => $setting->max_value,
                ]);

//                $newRoom->save(false);


                foreach ($roomUsers as $roomUser) {
                    // if(in_array($roomUser->rate_type, [RoomUser::RATE_TYPE_AWAIT_UP, RoomUser::RATE_TYPE_AWAIT_DOWN])){
                    if($roomUser->rate_type === RoomUser::RATE_TYPE_AWAIT_UP || $roomUser->rate_type === RoomUser::RATE_TYPE_AWAIT_DOWN || $roomUser->rate_type === RoomUser::RATE_TYPE_AUTO){
                        $roomUser->won = 2;
                        $roomUser->get_payment = 0;
                        $roomUser->write_off = 0;
                        $roomUser->datetime_end = date('Y-m-d H:i:s');
                        $user = User::findOne($roomUser->user_id);
                        echo $user->login.' - вернули<br>';
                        if($user){
                        	echo 'В условии<br>';
                            \Yii::warning("{$user->balance} + {$roomUser->rate}", 'count new balane');
                            $user->balance = $user->balance +  $roomUser->rate;
                            $user->reserve = $user->reserve - $roomUser->rate;
//                            $user->save(false);
                        }
                       $roomUser->save(false);
                       continue;
                        \Yii::warning($user->balance, 'first condition. saved user balance');
                    }

                    if ($roomUser->rate_type == $result) {
                        VarDumper::dump($roomUser->rate,10, true);
                        $a = ($sumFail / 100) * ($roomUser->rate / ($sumWin / 100));
                        VarDumper::dump($a, 10, true);
                        $roomUser->won = 1;
                        $roomUser->get_payment = $a;
                        $roomUser->write_off = 0;
                        $roomUser->datetime_end = date('Y-m-d H:i:s');
                       $user = User::findOne($roomUser->user_id);
                     	echo $user->login.' - победил<br>';
                        if ($user) {
                            \Yii::warning("{$user->balance} + {$a} + {$roomUser->rate}", 'count new balane');
                            $user->balance = $user->balance + $a + $roomUser->rate;
                            $user->reserve = $user->reserve - $roomUser->rate;
//                            \Yii::warning($user->save(false), 'user saved');
                        }
                       $roomUser->save(false);
                        \Yii::warning($user->balance, 'second condition. saved user balance');
                    } else {
                        $roomUser->won = 0;
                        $roomUser->get_payment = 0;
                        $roomUser->write_off = $roomUser->rate;
                        $roomUser->datetime_end = date('Y-m-d H:i:s');
                        $user = User::findOne($roomUser->user_id);
                        echo $user->login.' - проиграл<br>';
                        if ($user) {
//                            $user->balance -= $roomUser->rate;
                            $user->reserve = $user->reserve - $roomUser->rate;
//                            $user->save(false);
                        }
                       $roomUser->save(false);
                    }
                }
                
        }
    }

    /**
     *
     */
    public function actionEnd()
    {
        $models = Room::find()->where(['status' => Room::STATUS_AWAIT])->all();

        $a = (270 / 100) * (300 / (300 / 100));

//        echo $a;
//        exit;

        foreach ($models as $model)
        {
            if(date('Y-m-d H:i') >= date('Y-m-d H:i', strtotime($model->datetime_end))) {

                $url = 'https://www.bitstamp.net/api/v2/transactions/btcusd';

                $curl = curl_init($url);    //инициализируем curl по нашему урлу

                //        curl_setopt($curl, CURLOPT_PROXY, $proxy);

                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
                curl_setopt($curl, CURLOPT_HEADER, 1);
                $result = curl_exec($curl);   // исполняем сессию curl
                curl_close($curl); // завершаем сессию

                $result = substr($result, strpos($result, '['), strpos($result, ']'));
                //        var_dump( );
                $result = json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP

                $minutes = [];

                foreach ($result as $item) {
                    $date = date('Y-m-d H:i', $item->date);

                    if ($date == date('Y-m-d H:i', strtotime($model->datetime_end))) {
                        $minutes[] = $item->price;
                    }
                }

                $first = null;
                $last = null;

                if (count($minutes) > 0) {
                    $first = $minutes[0];
                    $last = $minutes[count($minutes) - 1];
                }

                $result = $first <= $last ? 1 : 0;


//                $result = rand(0, 1);

                $roomUsers = RoomUser::find()->where(['room_id' => $model->id])->all();

                $sumWin = RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => $result])->sum('rate');
                $sumWinCount = RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => $result])->count();
                $sumFail = RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => ($result == 1 ? 0 : 1)])->sum('rate');

                $resultCommission = ($sumFail / 100) * $model->service_commission;

                $sumFail = $sumFail - $resultCommission;
//                $sumWin = $sumWin / $sumWinCount;

//                $model->service_commission = $resultCommission;
                $model->balance = $resultCommission;
                $model->status = Room::STATUS_END;
                $model->save(false);

                $setting = RoomSetting::findOne($model->room_setting_id);

                $newRoom = new Room([
                    'name' => $setting->name,
                    'status' => Room::STATUS_ACCEPT,
                    'room_setting_id' => $setting->id,
                    'service_commission' => $setting->commission,
                    'diff_rate_one' => $setting->diff_rate_one,
                    'diff_rate_two' => $setting->diff_rate_two,
                    'datetime_accept_end' => date('Y-m-d H:i:s', time() + $setting->time_accept * 60),
                    'datetime_end' => date('Y-m-d H:i:s', time() + $setting->time_await * 60),
                    'min_value' => $setting->min_value,
                    'max_value' => $setting->max_value,
                ]);

                $newRoom->save(false);


                foreach ($roomUsers as $roomUser) {
                    if($roomUser->rate_type === RoomUser::RATE_TYPE_AWAIT_UP || $roomUser->rate_type === RoomUser::RATE_TYPE_AWAIT_DOWN || $roomUser->rate_type === RoomUser::RATE_TYPE_AUTO){
                        $roomUser->won = 2;
                        $roomUser->get_payment = 0;
                        $roomUser->write_off = 0;
                        $roomUser->datetime_end = date('Y-m-d H:i:s');
                        $user = User::findOne($roomUser->user_id);
                        if($user){
                            \Yii::warning("{$user->balance} + {$roomUser->rate}", 'count new balane');
                            $user->balance = $user->balance +  $roomUser->rate;
                            $user->reserve = $user->reserve - $roomUser->rate;
                            $user->save(false);
                        }
                        $roomUser->save(false);
                        continue;
                        \Yii::warning($user->balance, 'first condition. saved user balance');
                    }

                    if ($roomUser->rate_type == $result) {
                        $a = ($sumFail / 100) * ($roomUser->rate / ($sumWin / 100));
                        $roomUser->won = 1;
                        $roomUser->get_payment = $a;
                        $roomUser->write_off = 0;
                        $roomUser->datetime_end = date('Y-m-d H:i:s');
                        $user = User::findOne($roomUser->user_id);
                        if ($user) {
                            \Yii::warning("{$user->balance} + {$a} + {$roomUser->rate}", 'count new balane');
                            $user->balance = $user->balance + $a + $roomUser->rate;
                            $user->reserve = $user->reserve - $roomUser->rate;
                            \Yii::warning($user->save(false), 'user saved');
                        }
                        $roomUser->save(false);
                        \Yii::warning($user->balance, 'second condition. saved user balance');
                    } else {
                        $roomUser->won = 0;
                        $roomUser->get_payment = 0;
                        $roomUser->write_off = $roomUser->rate;
                        $roomUser->datetime_end = date('Y-m-d H:i:s');
                        $user = User::findOne($roomUser->user_id);
                        if ($user) {
//                            $user->balance -= $roomUser->rate;
                            $user->reserve = $user->reserve - $roomUser->rate;
                            $user->save(false);
                        }
                        $roomUser->save(false);
                    }
                }
            }
        }
    }
}
