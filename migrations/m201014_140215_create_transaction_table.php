<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transaction`.
 */
class m201014_140215_create_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'amount' => $this->float()->comment('Сумма'),
            'type' => $this->integer()->comment('Тип операции'),
            'status' => $this->integer()->comment('Статус'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
            'comment' => $this->text()->comment('Комментарий'),
        ]);

        $this->createIndex(
            'idx-transaction-user_id',
            'transaction',
            'user_id'
        );

        $this->addForeignKey(
            'fk-transaction-user_id',
            'transaction',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-transaction-user_id',
            'transaction'
        );

        $this->dropIndex(
            'idx-transaction-user_id',
            'transaction'
        );

        $this->dropTable('transaction');
    }
}
