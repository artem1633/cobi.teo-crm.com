<?php

use yii\db\Migration;

/**
 * Handles adding balance to table `room`.
 */
class m201104_084300_add_balance_column_to_room_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('room', 'balance', $this->float()->after('service_commission')->comment('Баланс площадки'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('room', 'balance');
    }
}
