<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m201006_115624_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);

        $this->insert('settings', [
            'key' => 'wallet_token',
            'value' => '',
            'label' => 'Токен кошелька',
        ]);

        $this->insert('settings', [
            'key' => 'additional_field_token',
            'value' => '',
            'label' => 'Доп поле кошелька',
        ]);

        $this->insert('settings', [
            'key' => 'curse_token',
            'value' => '',
            'label' => 'Токен курса',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
