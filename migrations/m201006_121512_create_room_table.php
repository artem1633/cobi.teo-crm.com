<?php

use yii\db\Migration;

/**
 * Handles the creation of table `room`.
 */
class m201006_121512_create_room_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('room', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'room_setting_id' => $this->integer()->comment('Настройка'),
            'datetime_accept_end' => $this->dateTime()->comment('Дата и время завершения приеам заявок'),
            'datetime_end' => $this->dateTime()->comment('Дата и время подведения итогов'),
            'service_commission' => $this->float()->comment('Коммисия сервиса'),
            'status' => $this->integer()->comment('Статус'),
            'min_value' => $this->float()->comment('Мин. значение'),
            'max_value' => $this->float()->comment('Макс. значение'),
            'diff_rate_one' => $this->float()->comment('Разница ставок 1'),
            'diff_rate_two' => $this->float()->comment('Разница ставок 2'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-room-room_setting_id',
            'room',
            'room_setting_id'
        );

        $this->addForeignKey(
            'fk-room-room_setting_id',
            'room',
            'room_setting_id',
            'room_setting',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-room-room_setting_id',
            'room'
        );

        $this->dropIndex(
            'idx-room-room_setting_id',
            'room'
        );

        $this->dropTable('room');
    }
}
