<?php

use yii\db\Migration;

/**
 * Handles adding wallet_id to table `user`.
 */
class m201014_131331_add_wallet_id_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'wallet_id', $this->string()->comment('Кошелек'));
        $this->addColumn('user', 'wallet_address', $this->string()->comment('Адрес кошелька'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('user', 'wallet_id');
        $this->dropIndex('user', 'wallet_address');
    }
}
