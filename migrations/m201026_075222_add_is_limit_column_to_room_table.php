<?php

use yii\db\Migration;

/**
 * Handles adding is_limit to table `room`.
 */
class m201026_075222_add_is_limit_column_to_room_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('room', 'is_limit', $this->boolean()->defaultValue(false)->comment('Лимит'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('room', 'is_limit');
    }
}
