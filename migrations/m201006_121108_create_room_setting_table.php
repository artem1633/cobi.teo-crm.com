<?php

use yii\db\Migration;

/**
 * Handles the creation of table `room_setting`.
 */
class m201006_121108_create_room_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('room_setting', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'prefix' => $this->string()->comment('Префикс'),
            'min_value' => $this->float()->comment('Мин значение'),
            'max_value' => $this->float()->comment('Макс значение'),
            'commission' => $this->float()->comment('Коммисия'),
            'diff_rate_one' => $this->float()->comment('Разница ставок 1'),
            'diff_rate_two' => $this->float()->comment('Разница ставок 2'),
            'time_accept' => $this->integer()->comment('Время приема заявкок'),
            'time_await' => $this->integer()->comment('Время ожидания результата'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('room_setting');
    }
}
