<?php

use yii\db\Migration;

/**
 * Handles the creation of table `room_user`.
 */
class m201006_122143_create_room_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('room_user', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'room_id' => $this->integer()->comment('Комната'),
            'datetime_enter' => $this->dateTime()->comment('Дата и время входа'),
            'rate' => $this->float()->comment('Ставка'),
            'rate_type' => $this->integer()->comment('На что ставка'),
            'won' => $this->boolean()->comment('Победил/Не победил'),
            'get_payment' => $this->float()->comment('Получил выплату'),
            'write_off' => $this->float()->comment('Списали'),
            'datetime_end' => $this->dateTime()->comment('Дата и время завершения'),
        ]);

        $this->createIndex(
            'idx-room_user-user_id',
            'room_user',
            'user_id'
        );

        $this->addForeignKey(
            'fk-room_user-user_id',
            'room_user',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-room_user-room_id',
            'room_user',
            'room_id'
        );

        $this->addForeignKey(
            'fk-room_user-room_id',
            'room_user',
            'room_id',
            'room',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-room_user-room_id',
            'room_user'
        );

        $this->dropIndex(
            'idx-room_user-room_id',
            'room_user'
        );

        $this->dropForeignKey(
            'fk-room_user-user_id',
            'room_user'
        );

        $this->dropIndex(
            'idx-room_user-user_id',
            'room_user'
        );

        $this->dropTable('room_user');
    }
}
