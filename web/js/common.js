$('#btn-dropdown_header').click(function(){
    if($(".dropdown-menu").is(':visible')){
        $(".dropdown-menu ").hide();
    } else {
        $(".dropdown-menu ").show();
    }
});

function copyToClipboard(text) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}


setInterval(function(){
    var hours = new Date().getHours();
    var minutes = new Date().getMinutes();

    // alert(hours+':'+minutes);

    $('[data-role="clock"]').text(hours+':'+minutes);
}, 5000);