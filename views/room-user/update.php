<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RoomUser */
?>
<div class="room-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
