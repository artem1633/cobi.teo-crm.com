<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RoomUser */
?>
<div class="room-user-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'room_id',
            'datetime_enter',
            'rate',
            'rate_type',
            'won',
            'get_payment',
            'write_off',
            'datetime_end',
        ],
    ]) ?>

</div>
