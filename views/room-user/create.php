<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RoomUser */

?>
<div class="room-user-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
