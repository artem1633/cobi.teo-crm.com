<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoomSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>

            <?=GridView::widget([
                'id'=>'crud-users-datatable',
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_users_columns.php'),
                'panelBeforeTemplate' => '',
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => ''
            ])?>
