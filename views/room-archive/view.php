<?php

use app\models\Room;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use app\models\Course;
use yii\widgets\Pjax;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $model app\models\Room */

$this->title = "Комната «{$model->name}»";

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<div class="room-view">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Комната</h4>
        </div>
        <div class="row">




            <!-- TradingView Widget END -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <?php if($model->status == 0): ?>
                                <?= Html::a('Подать заявку', ['room/add-user', 'room_id' => $model->id, 'reloadPjaxContainer' => '#crud-users-datatable-pjax'], ['class' => 'btn btn-primary', 'role' => 'modal-remote']) ?>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <?= $this->render('users_index', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                        ]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'name',
//                                [
//                                    'label' => 'Настройка',
//                                    'value' => 'roomSetting.name',
//                                ],
                                'datetime_accept_end',
                                'datetime_end',
                                'service_commission',
                                [
                                    'label' => 'Статус',
                                    'value' => function($model){
                                        return ArrayHelper::getValue(Room::statusLabels(), $model->status);
                                    }
                                ],
                                'min_value',
                                'max_value',
                                'diff_rate_one',
                                'diff_rate_two',
                                'created_at',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.setTimeout(function() {
            $('#f133').load('someScript.php');
        },100)
    </script>
    <script type="text/javascript">
        setInterval(function(){
            $.pjax.reload('#pjax-table-container');
        }, 60000);
        setInterval(function(){
            $.pjax.reload('#pjax-table2-container');
        }, 61000);
    </script>
    <?php Modal::begin([
        "id"=>"ajaxCrudModal",
        "footer"=>"",// always need it for jquery plugin
    ])?>
    <?php Modal::end(); ?>
