<?php
use app\models\Room;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\RoomUser;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Комнаты";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
    <div class="hidden">
        <div class="panel panel-inverse room-index">
            <div class="panel-heading">
                <!--        <div class="panel-heading-btn">-->
                <!--        </div>-->
                <h4 class="panel-title">Комнаты</h4>
            </div>
            <div class="panel-body">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns.php'),
                        'panelBeforeTemplate' => '',
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],
                            'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                        ["bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Вы уверены?',
                                            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
                                        ]),
                                ]).
                                '<div class="clearfix"></div>',
                        ]
                    ])?>
                </div>
            </div>
        </div>
    </div>

<?php \yii\widgets\Pjax::begin(['id' => 'pjax-card-container']) ?>
<?php foreach ($dataProvider->models as $model): ?>
    <div class="col-md-4">
        <?php

        $panelClass = "custom-panel";

        $roomUser = $model->getCurrentUserRate();

        if($roomUser){
            if($roomUser->rate_type == RoomUser::RATE_TYPE_UP){
                $panelClass .= ' panel-success';
            } else if($roomUser->rate_type == RoomUser::RATE_TYPE_DOWN) {
                $panelClass .= ' panel-danger';
            }
        }

        ?>
        <div class="<?=$panelClass?>">
            <div class="row">
                <div class="<?=(Yii::$app->user->identity->isSuperAdmin() ? 'col-md-10' : 'col-md-12')?>">
                    <h4><?= Html::a($model->name, ['room/view', 'id' => $model->id], ['data-pjax' => 0]) ?> <span class="pull-right" style="font-weight: 600;"><?= ArrayHelper::getValue(Room::statusLabels(), $model->statusByTime) ?></span></h4>
                    <div class="custom-panel-content">
                        <div class="row">
                            <div class="col-md-8">
                                <p>До завершения приема заявок осталось: <b><?php
                                        //                                            echo Yii::$app->formatter->format($model->datetime_accept_end, ['date', 'php:d.m.Y H:i']);
                                        echo $model->getUntilDateTimeAcceptEndText();
                                        ?></b></p>
                                <p>До подведения итогов осталось: <b><?php
                                        //                                            echo Yii::$app->formatter->format($model->datetime_end, ['date', 'php:d.m.Y H:i']);
                                        echo $model->getUntilDateTimeEndText();
                                        ?></b></p>
                                <p><?=$model->getAttributeLabel('created_at')?>: <b><?= Yii::$app->formatter->format($model->created_at, ['date', 'php:d.m.Y H:i']) ?></b></p>
                                <p>Сумма кр: <b><?php
                                        echo round(RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => RoomUser::RATE_TYPE_DOWN])->sum('rate'), 2);
                                        ?></b></p>
                            </div>
                            <div class="col-md-4">
                                <p>Кол-во пользователей: <b><?php
                                        $roomUsers = array_unique(ArrayHelper::getColumn(\app\models\RoomUser::find()->where(['room_id' => $model->id])->all(), 'user_id'));
                                        echo count($roomUsers);
                                        ?></b></p>
                                <p>Кол-во ставок: <b><?php
                                        echo \app\models\RoomUser::find()->where(['room_id' => $model->id])->count();
                                        ?></b></p>
                                <p>Сумма ставок: <b><?php
                                        echo round(\app\models\RoomUser::find()->where(['room_id' => $model->id])->sum('rate'), 2);
                                        ?></b></p>
                                <p>Сумма зел: <b><?php
                                        echo round(RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => RoomUser::RATE_TYPE_UP])->sum('rate'), 2);
                                        ?></b></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                    <div class="col-md-2">
                        <div class="custom-panel-btns pull-right">
                            <?= Html::a('<i class="fa fa-trash"></i>', ['room/delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-block pull-right',
                                'role'=>'modal-remote', 'title'=>'Удалить',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверены?',
                                'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                            ]); ?>
                            <?= Html::a('<i class="fa fa-pencil"></i>', ['room/update', 'id' => $model->id], [
                                'class' => 'btn btn-info btn-block pull-right',
                                'role'=>'modal-remote', 'title'=>'Изменить',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                            ]); ?>
                            <?= Html::a('<i class="fa fa-eye"></i>', ['room/view', 'id' => $model->id], [
                                'class' => 'btn btn-success btn-block pull-right',
                                'data-pjax' => 0,
                            ]); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php \yii\widgets\Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS
setInterval(function(){
    $.pjax.reload('#pjax-card-container');
}, 45000);
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>