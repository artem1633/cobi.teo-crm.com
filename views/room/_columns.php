<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Room;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($model){
            return Html::a($model->name, ['room/view', 'id' => $model->id], ['data-pjax' => 0]);
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'room_setting_id',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'datetime_accept_end',
        'format' => ['date', 'php:d.m.Y H:i:s'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'datetime_end',
        'format' => ['date', 'php:d.m.Y H:i:s'],
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'service_commission',
//    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'status',
         'value' => function($model){
            return ArrayHelper::getValue(Room::statusLabels(), $model->status);
         }
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'min_value',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'max_value',
     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'diff_rate_one',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'diff_rate_two',
//     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
         'format' => ['date', 'php:d.m.Y H:i:s'],
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Кол-во пользователей',
        'content' => function($model){
            $roomUsers = array_unique(ArrayHelper::getColumn(\app\models\RoomUser::find()->where(['room_id' => $model->id])->all(), 'user_id'));
            return count($roomUsers);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Кол-во ставок',
        'content' => function($model){
            return \app\models\RoomUser::find()->where(['room_id' => $model->id])->count();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Сумма ставок',
        'content' => function($model){
            return \app\models\RoomUser::find()->where(['room_id' => $model->id])->sum('rate');
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   