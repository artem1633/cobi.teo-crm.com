<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\RoomUser;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoomSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\Room */

$panelBeforeTemplate = '';



$all = RoomUser::find()->where(['room_id' => $model->id])->sum('rate');
$up =  RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => RoomUser::RATE_TYPE_UP])->sum('rate');
$down =  RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => RoomUser::RATE_TYPE_DOWN])->sum('rate');

$percent = $all / 100;
if($percent != 0) {
    $upPercent = round($up / $percent);
    $downPercent = round($down / $percent);
} else {
    $upPercent = 0;
    $downPercent = 0;
}

$panelBeforeTemplate .= "";

?>

            <?php

//            echo GridView::widget([
//                'id'=>'crud-users-datatable',
//                'dataProvider' => $dataProvider,
////                'filterModel' => $searchModel,
//                'pjax'=>true,
//                'columns' => require(__DIR__.'/_users_columns.php'),
//                'panelBeforeTemplate' => $panelBeforeTemplate,
//                'striped' => true,
//                'condensed' => true,
//                'responsive' => true,
//                'panel' => [
//                    'headingOptions' => ['style' => 'display: none;'],
//                    'after'=>''.
//                        '<div class="clearfix"></div>',
//                ]
//            ])
?>
