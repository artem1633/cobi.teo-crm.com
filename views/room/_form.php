<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Room */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'room_setting_id')->textInput() ?>

    <?= $form->field($model, 'datetime_accept_end')->textInput() ?>

    <?= $form->field($model, 'datetime_end')->textInput() ?>

    <?= $form->field($model, 'service_commission')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'min_value')->textInput() ?>

    <?= $form->field($model, 'max_value')->textInput() ?>

    <?= $form->field($model, 'diff_rate_one')->textInput() ?>

    <?= $form->field($model, 'diff_rate_two')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
