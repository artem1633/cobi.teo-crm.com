<?php

use app\models\Room;
use app\models\RoomUser;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use app\models\Course;
use yii\widgets\Pjax;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $model app\models\Room */

$this->title = "Комната «{$model->name}»";

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<div class="room-view">
    <div class="row">
        <div class="col-md-9">
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
                <div id="tradingview_f943e" style="
    height: 60vh;"></div>
                <div class="tradingview-widget-copyright"><a href="https://ru.tradingview.com/symbols/BTCUSD/?exchange=BITBAY" rel="noopener" target="_blank"><span class="blue-text">График BTCUSD</span></a> предоставлен TradingView</div>
                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                <script type="text/javascript">
                    new TradingView.widget(
                        {
                            "autosize": true,
                            "symbol": "BITSTAMP:BTCUSD",
                            "interval": "1",
                            "timezone": "Etc/UTC",
                            "theme": "dark",
                            "style": "1",
                            "locale": "ru",
                            "toolbar_bg": "#f1f3f6",
                            "enable_publishing": false,
                            "allow_symbol_change": true,
                            "container_id": "tradingview_f943e"
                        }
                    );
                </script>
            </div>
            <?php Pjax::begin(['id' => 'pjax-table-container', 'enablePushState' => false]) ?>
            <table class="table table-bordered" style="padding:0px;" data-update="graph-line">
                <thead>
                <tr>

                    <?php

                    $url = 'https://www.bitstamp.net/api/v2/transactions/btcusd';

                    $curl = curl_init($url);    //инициализируем curl по нашему урлу

                    //        curl_setopt($curl, CURLOPT_PROXY, $proxy);

                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
                    curl_setopt($curl, CURLOPT_HEADER, 1);
                    $result = curl_exec($curl);   // исполняем сессию curl
                    curl_close($curl); // завершаем сессию
                    //        if($decoded){
                    //        }
                    $result = substr($result, strpos($result, '[' ), strpos($result, ']' ));
                    //        var_dump( );
                    $result = json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
                    //							        $result = array_reverse($result);
                    //         var_dump(date('Y-m-d H:i:s',$result->timestamp));// если установили, значит декодируем полученную строку json формата в объект языка PHP
                    $minutes = [];
                    $arr = [];

                    if($result){
                        foreach ($result as $item) {


                            // if ($item->type == 0) {
                            //     $item->type = '<div style="background-color: #09f371;color: black; text-align: center;"> <span>Вверх</span></div>';

                            // } else {
                            //     $item->type = '<div style="background-color: #f10a20;color: black; text-align: center;"> <span>Вниз</span></div>';
                            // }

                            $now = date('Y-m-d H:i');
                            $date = date('Y-m-d H:i', $item->date);

                            if($date != $now)
                            {
                                if(isset($minutes[$date])){
                                    $minutes[$date][] = [
                                        'type' => $item->type,
                                        'price' => $item->price,
                                        'date' => date('Y-m-d H:i:s', $item->date),
                                    ];
                                } else {
                                    $minutes[$date] = [
                                        [
                                            'type' => $item->type,
                                            'price' => $item->price,
                                            'date' => date('Y-m-d H:i:s', $item->date),
                                        ],
                                    ];
                                }
                            }

//                            	if(in_array($date, $lastDates) == false){
                            // $arr[] = [
                            // 	'type' => $item->type,
                            // 	'price' => $item->price,
                            // 	'date' => date('Y-m-d H:i:s', $item->date),
                            // ];



//	                                $lastDates[] = $date;
//                            	}

                        }
                    }

                    //                            $arr = array_reverse($arr);

                    $counter = 0;
                    if(count($minutes) > 15){
                        $minutes = array_slice($minutes, 0, 15);
                    }
                    $minutes = array_reverse($minutes);
                    foreach ($minutes as $minute => $minuteDatas) {
                        $counter++;

                        $first = $minuteDatas[count($minuteDatas)-1];
                        $last = $minuteDatas[0];


                        // if ($first->type < 0) {
                        //     $item->type = '<div style="background-color: #09f371;color: black; text-align: center;"> <span>Вверх</span></div>';

                        // } else {
                        //     $item->type = '<div style="background-color: #f10a20;color: black; text-align: center;"> <span>Вниз</span></div>';
                        // }

                        $last['date'] = date('H:i', strtotime($last['date']));

                        if($first['price'] <= $last['price']){
                            $last['type'] = '<div style="background-color: #09f371;color: black; text-align: center;"> <span>'.$last['date'].'</span></div>';
                        } else {
                            $last['type'] = '<div style="background-color: #f10a20;color: black; text-align: center;"> <span>'.$last['date'].'</span></div>';
                        }

                        $last['date'] = date('Y-m-d H:i', strtotime($last['date']));

                        echo "
                                        <th>{$last['type']}</th>
                                    ";

//                                <th>{$last['price']} / {$first['price']}</th>
//                                        <th>{$last['date']}</th>



                        // echo "
                        //    <tr>
                        //        <td>{$first['type']}</td>
                        //        <td>{$first['price']}</td>
                        //        <td>{$first['date']}</td>

                        //    </tr>";


                    }
                    ?>
                    <!--                                <th>Направление</th>-->
                    <!--                                <th>Значение</th>-->
                    <!--                                <th>Дата и время</th>-->
                </tr>
                </thead>
                <tbody>


                </tbody>
            </table>
            <?php Pjax::end(); ?>
        </div>
        <div class="col-md-3">
            <div class="panel panel-inverse">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4><?=$model->name?></h4>
                        </div>
                        <div class="col-md-6">
                            <p>Time to stop: <b data-update="time-to-stop"><?=$model->getUntilDateTimeEndText();?></b></p>
                            <p>Round time: <b data-update="round-time"><?=date('H:i', strtotime($model->datetime_accept_end))?></b></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php Pjax::begin(['id' => 'pjax-order-container']) ?>
            <?php
            if (true) {
//                            echo Html::a('Подать заявку', ['room/add-user', 'room_id' => $model->id, 'reloadPjaxContainer' => '#crud-users-datatable-pjax'], ['class' => 'btn btn-primary', 'role' => 'modal-remote', 'style' => 'margin-top: 18px;']);
            }

            $all = RoomUser::find()->where(['room_id' => $model->id])->sum('rate');
            $up =  RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => [RoomUser::RATE_TYPE_UP, RoomUser::RATE_TYPE_AWAIT_UP]])->sum('rate');
            $down =  RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => [RoomUser::RATE_TYPE_DOWN, RoomUser::RATE_TYPE_AWAIT_DOWN]])->sum('rate');

            $small = 0;
            $big = 0;

            if($up > $down){
                $small = $down;
                $big = $up;
                $buy = "LIM";
                $sell = "ALL";
            } else if($up < $down){
                $small = $up;
                $big = $down;
                $buy = "ALL";
                $sell = "LIM";
            } else if($up == $down){
                $small = $up;
                $big = $down;
                $buy = "ALL";
                $sell = "ALL";
            }
            $needBalance = false;
            $diff = null;
            if($big > 0){
                $diff = round($small/$big, 2);

                if($model->is_limit == 1){
                    if($diff >= $model->diff_rate_two){
                        $model->is_limit = 0;
                        $buy = "ALL";
                        $sell = "ALL";
                        $needBalance = true;
                        $model->save(false);
                    }
                } else if($model->is_limit == 0){
                    if($diff <= $model->diff_rate_one){
                        $model->is_limit = 1;
                        $needBalance = false;
                        $model->save(false);
                    }
                }

                if($model->is_limit == 0){
                    $buy = "ALL";
                    $sell = "ALL";
                    $needBalance = true;
                }
//                        $model->save(false);


//                        if($diff < $model->diff_rate_one || $diff > $model->diff_rate_two){
//                            $needBalance = false;
//                        } else {
//                            $buy = "ALL";
//                            $sell = "ALL";
//                            $needBalance = true;
//                        }
            }









            ?>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Order</h4>
                </div>
                <div class="panel-body">

                    <?php
                    $lastRate = RoomUser::find()->where(['room_id' => $model->id, 'user_id' => Yii::$app->user->getId()])->one();
                    ?>


                    <p style='margin-bottom: 2px;' data-update="order-text">Вся сумма: <?=$all?>, соотношение: <?=$diff?>, нужен баланс: <?=($needBalance ? 'ДА' : 'НЕТ')?>, is_limit = <?=$model->is_limit?></p>
                    <!--                    <div style="width: 100%;" class="btn-group" role="group" aria-label="Basic example">-->
                    <button data-update="sell-btn" id="sell-rate-btn" style="width: 25%; font-size: 22px;" class="btn btn-danger"><span style="display: block; font-size: 14px;">Sell</span><?=$sell?></button>
                    <?php if($model->status == Room::STATUS_ACCEPT && $lastRate == null): ?>
                        <?= Html::a('-', '#', ['class' => 'btn btn-default btn-xs', 'data-number-down' => 'bid-fld']) ?>
                    <?php endif; ?>
                    <?php
                    $warnLabel = null;
                    if($lastRate != null){
                        $warnLabel = 'Вы уже поставили ставку';
                    }
                    if($model->status != Room::STATUS_ACCEPT){
                        $warnLabel = 'Прием окончен';
                    }
                    ?>
                    <?= Html::input('string', 'bid', ($warnLabel != null ? $warnLabel : $model->min_value), ['id' => 'bid-fld', 'class' => 'form-control', 'style' => 'display: inline-block; width: 30%;', 'min' => $model->min_value, 'max' => $model->max_value, 'disabled' => ($warnLabel != null ? true : false)]) ?>
                    <?php if($model->status == Room::STATUS_ACCEPT && $lastRate == null): ?>
                        <?= Html::a('+', '#', ['class' => 'btn btn-default btn-xs', 'data-number-up' => 'bid-fld']) ?>
                    <?php endif; ?>
                    <button data-update="buy-btn" id="buy-rate-btn" style="width: 25%; font-size: 22px;" class="btn btn-success"><span style="display: block; font-size: 14px;">Buy</span><?=$buy?></button>
                    <!--                    </div>-->
                </div>
            </div>

            <div class="panel panel-inverse hidden">
                <div class="panel-heading">
                    <h4 class="panel-title">Bid</h4>
                </div>
                <div class="panel-body">
                    <?php
                    $lastRate = RoomUser::find()->where(['room_id' => $model->id, 'user_id' => Yii::$app->user->getId()])->one();
                    ?>
                    <?php if($model->status == Room::STATUS_ACCEPT && $lastRate == null): ?>
                    <?php endif; ?>
                    <?php
                    $warnLabel = null;
                    if($lastRate != null){
                        $warnLabel = 'Вы уже поставили ставку';
                    }
                    if($model->status != Room::STATUS_ACCEPT){
                        $warnLabel = 'Прием окончен';
                    }
                    ?>
                    <?php if($model->status == Room::STATUS_ACCEPT && $lastRate == null): ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="panel panel-inverse hidden">
                <div class="panel-heading">
                    <h4 class="panel-title">Auto</h4>
                </div>
                <div class="panel-body">
                    <?php if($model->status == Room::STATUS_ACCEPT && $lastRate == null): ?>
                        <?= Html::a('-', '#', ['class' => 'btn btn-default btn-xs', 'style' => 'margin-left: 34px;', 'data-number-down' => 'auto-fld']) ?>
                    <?php endif; ?>
                    <?= Html::input('string', 'auto', ($warnLabel != null ? $warnLabel : 0), ['id' => 'auto-fld', 'class' => 'form-control', 'style' => 'display: inline-block; width: 55%;', 'min' => $model->min_value, 'max' => $model->max_value, 'disabled' => ($warnLabel != null ? true : false)]) ?>
                    <?php if($model->status == Room::STATUS_ACCEPT && $lastRate == null): ?>
                        <?= Html::a('+', '#', ['class' => 'btn btn-default btn-xs', 'data-number-up' => 'auto-fld']) ?>
                        <?= Html::a('Auto', '#', ['id' => 'auto-rate-btn', 'class' => 'btn btn-info btn-xs']) ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Balance</h4>
                </div>
                <div class="panel-body">
                    <h3 data-update="balance"><?=Yii::$app->user->identity->balance?> USDT</h3>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">My open orders & bid</h4>
                </div>
                <div class="panel-body">
                    <table style="width: 100%; font-size: 15px;">
                        <tbody data-update="open-orders-and-bid">
                        <?php

                        /** @var RoomUser[] $myRates */
                        $myRates = RoomUser::find()->where(['room_id' => $model->id, 'user_id' => Yii::$app->user->getId()])->all();

                        ?>
                        <?php foreach ($myRates as $rate): ?>
                            <tr>
                                <td>BIT</td>
                                <td><?=$rate->rate?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php Pjax::end() ?>
        </div>
        <div class="col-md-9" id="f133">

        </div>
    </div>





    <!-- TradingView Widget END -->
    <div class="row">
        <div class="col-md-10">
            <?php Pjax::begin(['id' => 'pjax-order-bid-container']) ?>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Order & Bid book</h4>
                </div>
                <div class="panel-body">
                    <?php if($model->statusByTime == Room::STATUS_ACCEPT): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?php

                                $usersCount = count(array_unique(ArrayHelper::getColumn($downDataProvider->models, 'user_id')));

                                ?>
                                <p data-update="book-danger" class="text-danger text-center" style="margin-bottom: 0; font-size: 17px; font-weight: 700;"><span style="font-size: 13px;"><?=$usersCount?>:</span><?=array_sum(ArrayHelper::getColumn($downDataProvider->models, 'rate'))?></p>
                                <?php

                                //                                echo GridView::widget([
                                //                                    'id' => 'crud-down-rates-datatable',
                                //                                    'dataProvider' => $downDataProvider,
                                //                                    //                'filterModel' => $searchModel,
                                //                                    'pjax' => false,
                                //                                    'columns' => require(__DIR__ . '/_rates_columns.php'),
                                //                                    'panelBeforeTemplate' => '<p class="text-danger text-center" style="margin-bottom: 0; font-size: 17px; font-weight: 700;">' . array_sum(ArrayHelper::getColumn($downDataProvider->models, 'rate')) . '</p>',
                                //                                    'striped' => true,
                                //                                    'condensed' => true,
                                //                                    'responsive' => true,
                                //                                    'panel' => [
                                //                                        'headingOptions' => ['style' => 'display: none;'],
                                //                                        'after' => '' .
                                //                                            '<div class="clearfix"></div>',
                                //                                    ]
                                //                                ])
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?php

                                $usersCount = count(array_unique(ArrayHelper::getColumn($upDataProvider->models, 'user_id')));

                                ?>
                                <p data-update="book-success" class="text-success text-center" style="margin-bottom: 0; font-size: 17px; font-weight: 700;"><span style="font-size: 13px;"><?=$usersCount?>:</span><?=array_sum(ArrayHelper::getColumn($upDataProvider->models, 'rate'))?></p>
                                <?php
                                //                                    echo GridView::widget([
                                //                                    'id' => 'crud-up-rates-datatable',
                                //                                    'dataProvider' => $upDataProvider,
                                //                                    //                'filterModel' => $searchModel,
                                //                                    'pjax' => false,
                                //                                    'columns' => require(__DIR__ . '/_rates_columns.php'),
                                //                                    'panelBeforeTemplate' => '<p class="text-success text-center" style="margin-bottom: 0; font-size: 17px; font-weight: 700;">' . array_sum(ArrayHelper::getColumn($upDataProvider->models, 'rate')) . '</p>',
                                //                                    'striped' => true,
                                //                                    'condensed' => true,
                                //                                    'responsive' => true,
                                //                                    'panel' => [
                                //                                        'headingOptions' => ['style' => 'display: none;'],
                                //                                        'after' => '' .
                                //                                            '<div class="clearfix"></div>',
                                //                                    ]
                                //                                ])
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?= $this->render('users_index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
            <?php Pjax::end() ?>
        </div>
        <div class="col-md-2">
            <div class="panel panel-inverse hidden">
                <div class="panel-heading">
                    <h4 class="panel-title">My 24H history</h4>
                </div>
                <div class="panel-body">
                    <table style="width: 100%; font-size: 15px;">
                        <tbody>
                        <tr>
                            <td>Order</td>
                            <td>55</td>
                            <td></td>
                            <td></td>
                            <td>12:10</td>
                        </tr>
                        <tr>
                            <td>Order</td>
                            <td>55</td>
                            <td>95%</td>
                            <td>17</td>
                            <td>12:10</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

$script = <<< JS
var socket = io('http://cobi.teo-crm.com:3002/?userId='+$('meta[name="user_id"]').attr('content'));

socket.on('update-rates', function(data){
    // $.pjax.reload('#crud-datatable-pjax');
    // setInterval(function(){
    //     $.pjax.reload('#report-messages-pjax');
    // }, 300);
    updateRoom('{$model->id}');
    // console.log(data, 'push from node');
});

function updateRoom(id){
    $.get('/room/get-info?id='+id, function(response){
        // console.log(response);
        $.each(response, function(i, row){

            $('[data-update="'+row.name+'"]').html(row.value);
        });
    });
}
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
<script type="text/javascript">
    window.setTimeout(function() {
        $('#f133').load('someScript.php');
    },100)
</script>
<script type="text/javascript">
    setInterval(function(){
//        $.pjax.reload('#pjax-table-container');
    }, 60000);
    //    setInterval(function(){
    //        $.pjax.reload('#pjax-table2-container');
    //    }, 61000);

    function checkValueRange(fld, min, max)
    {
        var value = parseInt($(fld).val());
        if(value < min){
            $(fld).val(min);
        } else if(value > max){
            $(fld).val(max);
        }
    }

    function handleRateResponse(response)
    {
        if(response.result == true){
//            $.pjax.reload('#pjax-order-bid-container');
            setTimeout(function(){
//                $.pjax.reload('#pjax-order-container');
            }, 1000);
        } else {
            alert(response.message);
        }
    }

    $('[data-number-down]').click(function(e){
        e.preventDefault();
        var fldId = $(this).data('number-down');
        var value = parseInt($('#'+fldId).val());
        var min = $('#'+fldId).attr('min');
        var max = $('#'+fldId).attr('max');
        value = value - 1;
        $('#'+fldId).val(value);
        checkValueRange('#'+fldId, min, max);
    });
    $('[data-number-up]').click(function(e){
        e.preventDefault();
        var fldId = $(this).data('number-up');
        var value = parseInt($('#'+fldId).val());
        var value = parseInt($('#'+fldId).val());
        var min = $('#'+fldId).attr('min');
        var max = $('#'+fldId).attr('max');
        value = value + 1;
        $('#'+fldId).val(value);
        checkValueRange('#'+fldId, min, max);
    });
    $('#bid-fld, #auto-fld').keyup(function(){
        var min = $(this).attr('min');
        var max = $(this).attr('max');
        checkValueRange(this, min, max);
    });

    $('#sell-rate-btn').click(function(e){
        e.preventDefault();

        var value = $('#bid-fld').val();

        $.get('/room/sell?id=<?=$model->id?>&rate='+value, function(response){
            handleRateResponse(response);
        });
    });

    $('#buy-rate-btn').click(function(e){
        e.preventDefault();

        var value = $('#bid-fld').val();

        $.get('/room/buy?id=<?=$model->id?>&rate='+value, function(response){
            handleRateResponse(response);
        });
    });

    $('#auto-rate-btn').click(function(e){
        e.preventDefault();

        var value = $('#auto-fld').val();

        $.get('/room/auto?id=<?=$model->id?>&rate='+value, function(response){
            handleRateResponse(response);
        });
    });

</script>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
