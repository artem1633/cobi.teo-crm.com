<?php
use app\models\RoomUser;

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rate',
        'content' => function($model){
            if($model->rate_type == RoomUser::RATE_TYPE_UP){
                return "<span class='text-success'>{$model->rate}</span>";
            } else if($model->rate_type == RoomUser::RATE_TYPE_DOWN) {
                return "<span class='text-danger'>{$model->rate}</span>";
            }
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Очередь',
        'content' => function($model){
            if($model->rate_type == RoomUser::RATE_TYPE_AWAIT_UP){
                return "<span class='text-success'>{$model->rate}</span>";
            } else if($model->rate_type == RoomUser::RATE_TYPE_AWAIT_DOWN) {
                return "<span class='text-danger'>{$model->rate}</span>";
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'datetime_enter',
        'format' => ['date', 'php:d.m.Y H:i:s'],
    ],
];