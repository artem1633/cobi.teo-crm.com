<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RoomSetting */

?>
<div class="room-setting-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
