<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RoomSetting */
?>
<div class="room-setting-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'prefix',
            'min_value',
            'max_value',
            'commission',
            'diff_rate_one',
            'diff_rate_two',
            'time_accept:datetime',
            'time_await:datetime',
        ],
    ]) ?>

</div>
