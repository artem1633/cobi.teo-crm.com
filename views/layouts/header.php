<?php

use yii\helpers\Html;
use app\models\Users;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header">
                    <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                        <span class="navbar-logo"></span>
                        <?=Yii::$app->name?>
                    </a>
                    <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- end mobile sidebar expand / collapse button -->
                
                <?php if(Yii::$app->user->isGuest == false): ?>
                    <!-- begin header navigation right -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown navbar-user">
                            <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-xs"><?=Yii::$app->user->identity->login?></span> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeInLeft">
                                <li class="arrow"></li>
                                <li> <?= Html::a('Сменить пароль', ['site/reset-password'], ['role' => 'modal-remote', 'data-pjax' => 1]) ?> </li>
                                <li class="divider"></li>
                                <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown navbar-user">
                            <p style="margin-top: 18px;">Время: <b data-role="clock"><?= date('H:i') ?></b>, Баланс: <b data-update="header-balance"><?= Yii::$app->user->identity->balance ?></b>, Резерв: <b data-update="header-reserve"><?= Yii::$app->user->identity->reserve ?></b></p>
                        </li>
                    </ul>
                    <!-- end header navigation right -->
                <?php endif; ?>
            </div>
            <!-- end container-fluid -->
        </div>