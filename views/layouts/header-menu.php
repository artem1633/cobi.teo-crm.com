<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Комнаты', 'icon' => 'fa  fa-building', 'url' => ['/room'],],
                    ['label' => 'История ставок', 'icon' => 'fa  fa-bank', 'url' => ['/room-user'],],
                    ['label' => 'Финансы', 'icon' => 'fa fa-rub', 'url' => ['/transaction'],],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Настройки', 'icon' => 'fa  fa-cog', 'url' => ['/settings'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Настройки комнаты', 'icon' => 'fa  fa-cogs', 'url' => ['/room-setting'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Архив', 'icon' => 'fa  fa-archive', 'url' => ['/room-archive'],],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
