<?php

namespace app\controllers;

use app\models\RoomSetting;
use app\models\RoomUser;
use app\models\RoomUserSearch;
use app\models\User;
use Yii;
use app\models\Room;
use app\models\RoomSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * RoomController implements the CRUD actions for Room model.
 */
class RoomController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Room models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoomSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['!=', 'status', Room::STATUS_END]);

        $dataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Room model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;

        $searchModel = new RoomUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['room_id' => $id]);

        $upSearchModel = new RoomUserSearch();
        $upDataProvider = $upSearchModel->search(Yii::$app->request->queryParams);
        $upDataProvider->query->andWhere(['room_id' => $id, 'rate_type' => [RoomUser::RATE_TYPE_UP, RoomUser::RATE_TYPE_AWAIT_UP]]);

        $downSearchModel = new RoomUserSearch();
        $downDataProvider = $downSearchModel->search(Yii::$app->request->queryParams);
        $downDataProvider->query->andWhere(['room_id' => $id, 'rate_type' => [RoomUser::RATE_TYPE_DOWN, RoomUser::RATE_TYPE_AWAIT_DOWN]]);


        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $dataProvider->query->andWhere(['user_id' => Yii::$app->user->getId()]);
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'title'=> "комната #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'upSearchModel' => $upSearchModel,
                    'upDataProvider' => $upDataProvider,
                    'downSearchModel' => $downSearchModel,
                    'downDataProvider' => $downDataProvider,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'upSearchModel' => $upSearchModel,
                'upDataProvider' => $upDataProvider,
                'downSearchModel' => $downSearchModel,
                'downDataProvider' => $downDataProvider,
            ]);
        }
    }

    /**
     * Creates a new Room model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Room();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить комнату",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#pjax-card-container',
                    'title'=> "Добавить комнату",
                    'content'=>'<span class="text-success">Создание комнаты успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Добавить комнату",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Room model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить комнату #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#pjax-card-container',
                    'forceClose' => true,
                ];
            }else{
                return [
                    'title'=> "Изменить комнату #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param int $id
     * @param string $rate
     * @return mixed
     */
    public function actionBuy($id, $rate)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $roomUser = new RoomUser([
            'room_id' => $id,
            'user_id' => Yii::$app->user->getId(),
            'rate' => $rate,
            'rate_type' => RoomUser::RATE_TYPE_UP,
        ]);

        if(time() > strtotime($model->datetime_accept_end)){
            return ['result' => false, 'message' => 'Прием ставок окончен'];
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if(($user->balance - $rate) < 0){
            return ['result' => false, 'message' => 'Недостаточно средств'];
        }

        $last = RoomUser::find()->where(['room_id' => $id, 'user_id' => Yii::$app->user->getId()])->one();

        if($last){
            return ['result' => false, 'message' => 'Вы уже поставили ставку'];
        }

        $user->balance = $user->balance - $rate;
        $user->reserve = $user->reserve + $rate;
        $user->save(false);
        $roomUser->save(false);

        $up =  RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => [RoomUser::RATE_TYPE_UP, RoomUser::RATE_TYPE_AWAIT_UP]])->sum('rate');
        $down =  RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => [RoomUser::RATE_TYPE_DOWN, RoomUser::RATE_TYPE_AWAIT_DOWN]])->sum('rate');

        \Yii::warning("IN THE CONDITION");

        $small = 0;
        $big = 0;

        if($up > $down){
            $small = $down;
            $big = $up;
            $lim = RoomUser::RATE_TYPE_DOWN;
        } else if($up < $down){
            $small = $up;
            $big = $down;
            $lim = RoomUser::RATE_TYPE_UP;
        } else if($up == $down){
            $small = $up;
            $big = $down;
            $buy = "ALL";
            $sell = "ALL";
            $lim = null;
        }
        $needBalance = false;
        $diff = null;
        if($big > 0){
            $diff = round($small/$big, 2);

            if($model->is_limit == 1){
                if($diff >= $model->diff_rate_two){
                    $model->is_limit = 0;
                    $model->save(false);
                }
            } else if($model->is_limit == 0){
                if($diff <= $model->diff_rate_one){
                    $model->is_limit = 1;
                    $model->save(false);
                }
            }
            if($model->is_limit == 0){
                $lim = null;
            }

//                if($diff < $model->diff_rate_one || $diff > $model->diff_rate_two){
//                    $needBalance = true;
//                } else {
//                    $lim = null;
//                }
        }

        \Yii::warning($lim, 'lim value');
        if($lim !== null){
            \Yii::warning($lim, 'LIM');
            $autoRate = RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => RoomUser::RATE_TYPE_AUTO])->one();
            if($autoRate){
                $autoRate->rate_type = $lim;
                $autoRate->save(false);
            } else {
                $searchStatus = null;
                if($lim == RoomUser::RATE_TYPE_UP){
                    $searchStatus = RoomUser::RATE_TYPE_AWAIT_UP;
                }
                if($lim == RoomUser::RATE_TYPE_DOWN){
                    $searchStatus = RoomUser::RATE_TYPE_AWAIT_DOWN;
                }
                $waiting = RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => $searchStatus])->one();
                if($waiting){
                    if($waiting->rate_type == RoomUser::RATE_TYPE_AWAIT_UP){
                        $waiting->rate_type = RoomUser::RATE_TYPE_UP;
                    } else if($waiting->rate_type == RoomUser::RATE_TYPE_AWAIT_DOWN){
                        $waiting->rate_type = RoomUser::RATE_TYPE_DOWN;
                    }
                    $waiting->save(false);
                }
            }
        } else {
            $waitingsBuy = RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => [RoomUser::RATE_TYPE_AWAIT_DOWN, RoomUser::RATE_TYPE_AWAIT_UP]])->all();
            foreach ($waitingsBuy as $waiting){
                if($waiting->rate_type == RoomUser::RATE_TYPE_AWAIT_UP){
                    $waiting->rate_type = RoomUser::RATE_TYPE_UP;
                } else if($waiting->rate_type == RoomUser::RATE_TYPE_AWAIT_DOWN){
                    $waiting->rate_type = RoomUser::RATE_TYPE_DOWN;
                }
                $waiting->save(false);
            }
        }

        Yii::$app->elephantio->emit('rate-update', ['param1' => 'value1']);

        return ['result' => true];
    }

    /**
     * @param int $id
     * @param string $rate
     * @return mixed
     */
    public function actionSell($id, $rate)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $roomUser = new RoomUser([
            'room_id' => $id,
            'user_id' => Yii::$app->user->getId(),
            'rate' => $rate,
            'rate_type' => RoomUser::RATE_TYPE_DOWN,
        ]);

        if(time() > strtotime($model->datetime_accept_end)){
            return ['result' => false, 'message' => 'Прием ставок окончен'];
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if(($user->balance - $rate) < 0){
            return ['result' => false, 'message' => 'Недостаточно средств'];
        }

        $last = RoomUser::find()->where(['room_id' => $id, 'user_id' => Yii::$app->user->getId()])->one();

        if($last){
            return ['result' => false, 'message' => 'Вы уже поставили ставку'];
        }

        $user->balance = $user->balance - $rate;
        $user->reserve = $user->reserve + $rate;
        $user->save(false);

        $roomUser->save(false);

        $up =  RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => [RoomUser::RATE_TYPE_UP, RoomUser::RATE_TYPE_AWAIT_UP]])->sum('rate');
        $down =  RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => [RoomUser::RATE_TYPE_DOWN, RoomUser::RATE_TYPE_AWAIT_DOWN]])->sum('rate');

        \Yii::warning("IN THE CONDITION");

        $small = 0;
        $big = 0;

        if($up > $down){
            $small = $down;
            $big = $up;
            $lim = RoomUser::RATE_TYPE_DOWN;
        } else if($up < $down){
            $small = $up;
            $big = $down;
            $lim = RoomUser::RATE_TYPE_UP;
        } else if($up == $down){
            $small = $up;
            $big = $down;
            $buy = "ALL";
            $sell = "ALL";
            $lim = null;
        }
        $needBalance = false;
        $diff = null;
        if($big > 0){
            $diff = round($small/$big, 2);

            if($model->is_limit == 1){
                if($diff >= $model->diff_rate_two){
                    $model->is_limit = 0;
                    $model->save(false);
                }
            } else if($model->is_limit == 0){
                if($diff <= $model->diff_rate_one){
                    $model->is_limit = 1;
                    $model->save(false);
                }
            }
            if($model->is_limit == 0){
                $lim = null;
            }

//            if($diff < $model->diff_rate_one || $diff > $model->diff_rate_two){
//                $needBalance = true;
//            } else {
//                $lim = null;
//            }
        }

        \Yii::warning($lim, 'lim value');
        if($lim !== null){
            \Yii::warning($lim, 'LIM');
            $autoRate = RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => RoomUser::RATE_TYPE_AUTO])->one();
            if($autoRate){
                $autoRate->rate_type = $lim;
                $autoRate->save(false);
            } else {
                $searchStatus = null;
                if($lim == RoomUser::RATE_TYPE_UP){
                    $searchStatus = RoomUser::RATE_TYPE_AWAIT_UP;
                }
                if($lim == RoomUser::RATE_TYPE_DOWN){
                    $searchStatus = RoomUser::RATE_TYPE_AWAIT_DOWN;
                }
                $waiting = RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => $searchStatus])->one();
                if($waiting){
                    if($waiting->rate_type == RoomUser::RATE_TYPE_AWAIT_UP){
                        $waiting->rate_type = RoomUser::RATE_TYPE_UP;
                    } else if($waiting->rate_type == RoomUser::RATE_TYPE_AWAIT_DOWN){
                        $waiting->rate_type = RoomUser::RATE_TYPE_DOWN;
                    }
                    $waiting->save(false);
                }
            }
        } else {
            $waitingsSell = RoomUser::find()->where(['room_id' => $roomUser->room_id, 'rate_type' => [RoomUser::RATE_TYPE_AWAIT_UP, RoomUser::RATE_TYPE_AWAIT_DOWN]])->all();
            foreach ($waitingsSell as $waiting){
                if($waiting->rate_type == RoomUser::RATE_TYPE_AWAIT_UP){
                    $waiting->rate_type = RoomUser::RATE_TYPE_UP;
                } else if($waiting->rate_type == RoomUser::RATE_TYPE_AWAIT_DOWN){
                    $waiting->rate_type = RoomUser::RATE_TYPE_DOWN;
                }
                $waiting->save(false);
            }
        }

        Yii::$app->elephantio->emit('rate-update', ['param1' => 'value1']);

        return ['result' => true];
    }

    /**
     * @param int $id
     * @param string $rate
     * @return mixed
     */
    public function actionAuto($id, $rate)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $roomUser = new RoomUser([
            'room_id' => $id,
            'user_id' => Yii::$app->user->getId(),
            'rate' => $rate,
            'rate_type' => RoomUser::RATE_TYPE_AUTO,
        ]);

        if(time() > strtotime($model->datetime_accept_end)){
            return ['result' => false, 'message' => 'Прием ставок окончен'];
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;
        if(($user->balance - $rate) < 0){
            return ['result' => false, 'message' => 'Недостаточно средств'];
        }
        $user->balance = $user->balance - $rate;
        $user->reserve = $user->reserve + $rate;
        $user->save(false);

        return ['result' => $roomUser->save()];
    }

    public function actionGetInfo($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $upSearchModel = new RoomUserSearch();
        $upDataProvider = $upSearchModel->search(Yii::$app->request->queryParams);
        $upDataProvider->query->andWhere(['room_id' => $id, 'rate_type' => [RoomUser::RATE_TYPE_UP, RoomUser::RATE_TYPE_AWAIT_UP]]);

        $downSearchModel = new RoomUserSearch();
        $downDataProvider = $downSearchModel->search(Yii::$app->request->queryParams);
        $downDataProvider->query->andWhere(['room_id' => $id, 'rate_type' => [RoomUser::RATE_TYPE_DOWN, RoomUser::RATE_TYPE_AWAIT_DOWN]]);


        $all = RoomUser::find()->where(['room_id' => $model->id])->sum('rate');
        $up =  RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => [RoomUser::RATE_TYPE_UP, RoomUser::RATE_TYPE_AWAIT_UP]])->sum('rate');
        $down =  RoomUser::find()->where(['room_id' => $model->id, 'rate_type' => [RoomUser::RATE_TYPE_DOWN, RoomUser::RATE_TYPE_AWAIT_DOWN]])->sum('rate');

        $small = 0;
        $big = 0;

        if($up > $down){
            $small = $down;
            $big = $up;
            $buy = "LIM";
            $sell = "ALL";
        } else if($up < $down){
            $small = $up;
            $big = $down;
            $buy = "ALL";
            $sell = "LIM";
        } else if($up == $down){
            $small = $up;
            $big = $down;
            $buy = "ALL";
            $sell = "ALL";
        }
        $needBalance = false;
        $diff = null;
        if($big > 0){
            $diff = round($small/$big, 2);

            if($model->is_limit == 1){
                if($diff >= $model->diff_rate_two){
                    $model->is_limit = 0;
                    $buy = "ALL";
                    $sell = "ALL";
                    $needBalance = true;
                    $model->save(false);
                }
            } else if($model->is_limit == 0){
                if($diff <= $model->diff_rate_one){
                    $model->is_limit = 1;
                    $needBalance = false;
                    $model->save(false);
                }
            }

            if($model->is_limit == 0){
                $buy = "ALL";
                $sell = "ALL";
                $needBalance = true;
            }
//                        $model->save(false);


//                        if($diff < $model->diff_rate_one || $diff > $model->diff_rate_two){
//                            $needBalance = false;
//                        } else {
//                            $buy = "ALL";
//                            $sell = "ALL";
//                            $needBalance = true;
//                        }
        }

        $orderText = 'Вся сумма: '.$all.', соотношение: '.$diff.', нужен баланс: '.($needBalance ? 'ДА' : 'НЕТ').', is_limit = '.$model->is_limit;

        $sellBtn = '<span style="display: block; font-size: 14px;">Sell</span>'.$sell;
        $buyBtn = '<span style="display: block; font-size: 14px;">Buy</span>'.$buy;


        $usersCount = count(array_unique(ArrayHelper::getColumn($downDataProvider->models, 'user_id')));
        $bookDanger = '<span style="font-size: 13px;">'.$usersCount.':</span>'.array_sum(ArrayHelper::getColumn($downDataProvider->models, 'rate'));

        $usersCount = count(array_unique(ArrayHelper::getColumn($upDataProvider->models, 'user_id')));
        $bookSuccess = '<span style="font-size: 13px;">'.$usersCount.':</span>'.array_sum(ArrayHelper::getColumn($upDataProvider->models, 'rate'));

        $timeToStop = $model->getUntilDateTimeEndText();
        $roundTime = date('H:i', strtotime($model->datetime_accept_end));

        $headerBalance = Yii::$app->user->identity->balance;
        $headerReserve = Yii::$app->user->identity->reserve;

        $balance = $headerBalance.' USDT';

        $openOrdersAndBid = '';

        /** @var RoomUser[] $myRates */
        $myRates = RoomUser::find()->where(['room_id' => $model->id, 'user_id' => Yii::$app->user->getId()])->all();

        foreach($myRates as $rate)
        {
            $openOrdersAndBid .= "<tr>
                            <td>BIT</td>
                            <td>{$rate->rate}</td>
                        </tr>";
        }

        $graphLine = $this->renderPartial('graph-line');

        $income = "{$big} {$small}";

        return [
            [
                'name' => 'graph-line',
                'value' => $graphLine,
            ],
            [
                'name' => 'income',
                'value' => $income,
            ],
            [
                'name' => 'open-orders-and-bid',
                'value' => $openOrdersAndBid,
            ],
            [
                'name' => 'balance',
                'value' => $balance,
            ],
            [
                'name' => 'header-balance',
                'value' => $headerBalance,
            ],
            [
                'name' => 'header-reserve',
                'value' => $headerReserve,
            ],
            [
                'name' => 'time-to-stop',
                'value' => $timeToStop,
            ],
            [
                'name' => 'round-time',
                'value' => $roundTime,
            ],
            [
                'name' => 'book-danger',
                'value' => $bookDanger,
            ],
            [
                'name' => 'book-success',
                'value' => $bookSuccess,
            ],
            [
                'name' => 'buy-btn',
                'value' => $buyBtn,
            ],
            [
                'name' => 'order-text',
                'value' => $orderText,
            ],
            [
                'name' => 'sell-btn',
                'value' => $sellBtn,
            ],
            [
                'name' => 'buy-btn',
                'value' => $buyBtn,
            ],
        ];
    }


    /**
     *
     */
    public function actionAutoCreate()
    {
        /** @var RoomSetting $settings */
        $settings = RoomSetting::find()->all();

        foreach ($settings as $setting)
        {
            $name = $setting->prefix.date('dmy', time() + $setting->time_await * 60);


            $model = new Room([
                'name' => $name,
                'status' => Room::STATUS_ACCEPT,
                'room_setting_id' => $setting->id,
                'service_commission' => $setting->commission,
                'diff_rate_one' => $setting->diff_rate_one,
                'diff_rate_two' => $setting->diff_rate_two,
                'datetime_accept_end' => date('Y-m-d H:i:s', time() + $setting->time_accept * 60),
                'datetime_end' => date('Y-m-d H:i:s', time() + $setting->time_await * 60),
                'min_value' => $setting->min_value,
                'max_value' => $setting->max_value,
            ]);

            $model->save(false);
        }
    }

    /**
     * @param int $room_id
     * @param string $reloadPjaxContainer
     * @return array|string|Response
     */
    public function actionAddUser($room_id, $reloadPjaxContainer = '#pjax-card-container')
    {
        $request = Yii::$app->request;
        $model = new RoomUser();
        $model->room_id = $room_id;
        $model->user_id = Yii::$app->user->getId();
        $room = $this->findModel($room_id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if(strtotime($room->datetime_accept_end) < time()){
            return [
                'title'=> "Подать заявку",
                'content'=> '<span class="text-danger">Извините. Время подачи заявок истекло</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"]),

            ];
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Подать заявку",
                    'content'=>$this->renderAjax('@app/views/room-user/create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                /** @var User $user */
                $user = Yii::$app->user->identity;
                $user->balance = $user->balance - $model->rate;
                $user->reserve = $user->reserve + $model->rate;
                $user->save(false);

                return [
                    'forceReload'=>$reloadPjaxContainer,
                    'title'=> "Подать заявку",
                    'content'=>'<span class="text-success">Заявка успешно создана</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"]),
                ];
            }else{
                return [
                    'title'=> "Подать заявку",
                    'content'=>$this->renderAjax('@app/views/room-user/create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Room model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#pjax-card-container'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Room model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#pjax-card-container'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Room model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Room the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Room::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
