<?php

namespace app\controllers;

use app\models\forms\ResetPasswordForm;
use app\models\forms\SignupForm;
use app\models\Settings;
use GuzzleHttp\Client;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTest()
    {
        Yii::$app->elephantio->emit('rate-update', ['param1' => 'value1']);
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionRegister()
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && ($user = $model->signup())) {
            \Yii::warning($model->login.' '.$model->password);
            (new LoginForm(['username' => $model->login, 'password' => $model->password]))->login();

            $apiKey = Settings::findByKey('wallet_token')->value;
            if($apiKey){
                $ch = curl_init('https://cryptoprocessing.io/api/v1/wallets');
                $data = [
                    "name" => "crypto_usdt_".Yii::$app->security->generateRandomString(10)."_{$user->id}",
                    'currency' => "USDTETH",
                    "human" => "{$model->login} ({$user->id})",
                    'description' => 'usdt user wallet'
                ];
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Token {$apiKey}", "Content-Type: application/json", "Idempotency-Key: ".Yii::$app->security->generateRandomString(20)]);
                $result = json_decode(curl_exec($ch), true);
                \Yii::warning($result, 'Result from API');
                curl_close($ch);

                if(isset($result['data'])){
                    $user->wallet_id = $result['data']['id'];

                    $ch = curl_init("https://cryptoprocessing.io/api/v1/wallets/{$result['data']['id']}/addresses");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Token {$apiKey}", "Content-Type: application/json"]);
                    $result = json_decode(curl_exec($ch), true);
                    \Yii::warning($result, 'Result from API');
                    curl_close($ch);

                    if(isset($result['data'][0]['address'])){
                        $user->wallet_address = $result['data'][0]['address'];
                    }

                    $user->save(false);

                    //set web hook
                    $ch = curl_init("https://cryptoprocessing.io/api/v1/wallets/{$user->wallet_id}/webhooks");

                    $data = [
                        "url" => Url::toRoute(['api/room/webhook'], true),
                        'max_confirmations' => "6",
                        "max_retries" => "2",
                    ];

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Token {$apiKey}", "Content-Type: application/json"]);

                    $result = json_decode(curl_exec($ch), true);
                    Yii::warning($result, 'Response from API');

                    curl_close($ch);
                }
            }

            return $this->goHome();
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }
}
