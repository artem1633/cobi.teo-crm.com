<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RoomUser;

/**
 * RoomUserSearch represents the model behind the search form about `app\models\RoomUser`.
 */
class RoomUserSearch extends RoomUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'room_id', 'rate_type', 'won'], 'integer'],
            [['datetime_enter', 'datetime_end'], 'safe'],
            [['rate', 'get_payment', 'write_off'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RoomUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'room_id' => $this->room_id,
            'datetime_enter' => $this->datetime_enter,
            'rate' => $this->rate,
            'rate_type' => $this->rate_type,
            'won' => $this->won,
            'get_payment' => $this->get_payment,
            'write_off' => $this->write_off,
            'datetime_end' => $this->datetime_end,
        ]);

        return $dataProvider;
    }
}
