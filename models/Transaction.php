<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property double $amount Сумма
 * @property int $type Тип операции
 * @property int $status Статус
 * @property string $datetime Дата и время
 * @property string $comment Комментарий
 *
 * @property User $user
 */
class Transaction extends \yii\db\ActiveRecord
{
    const TYPE_SEND = 0;
    const TYPE_RECEIVE = 1;

    const STATUS_WORKING = 0;
    const STATUS_DONE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'datetime',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'user_id',
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'status'], 'integer'],
            [['amount'], 'number'],
            [['amount'], function(){
                if($this->amount > Yii::$app->user->identity->balance){
                    $this->addError('amount', 'Недостаточно средств');
                }
            }],
            [['datetime'], 'safe'],
            [['comment'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'amount' => 'Сумма',
            'type' => 'Тип операции',
            'status' => 'Статус',
            'datetime' => 'Дата и время',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return array
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_SEND => 'Списание',
            self::TYPE_RECEIVE => 'Поступление'
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_WORKING => 'В работе',
            self::STATUS_DONE => 'Выполнен'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
