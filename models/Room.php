<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "room".
 *
 * @property int $id
 * @property string $name Название
 * @property int $room_setting_id Настройка
 * @property string $datetime_accept_end Дата и время завершения приеам заявок
 * @property string $datetime_end Дата и время подведения итогов
 * @property double $service_commission Коммисия сервиса
 * @property double $balance Баланс площадки
 * @property int $status Статус
 * @property double $min_value Мин. значение
 * @property double $max_value Макс. значение
 * @property double $diff_rate_one Разница ставок 1
 * @property double $diff_rate_two Разница ставок 2
 * @property string $created_at
 * @property int $is_limit
 *
 * @property int $statusByTime
 *
 * @property RoomSetting $roomSetting
 * @property RoomUser[] $roomUsers
 */
class Room extends \yii\db\ActiveRecord
{
    const STATUS_ACCEPT = 0;
    const STATUS_AWAIT = 1;
    const STATUS_END = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_setting_id', 'status', 'is_limit'], 'integer'],
            [['datetime_accept_end', 'datetime_end', 'created_at'], 'safe'],
            [['service_commission', 'min_value', 'max_value', 'diff_rate_one', 'diff_rate_two', 'balance'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['room_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => RoomSetting::className(), 'targetAttribute' => ['room_setting_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'room_setting_id' => 'Настройка',
            'datetime_accept_end' => 'Дата и время завершения приема заявок',
            'datetime_end' => 'Дата и время подведения итогов',
            'service_commission' => 'Коммисия сервиса',
            'balance' => 'Баланс площадки',
            'status' => 'Статус',
            'min_value' => 'Мин. значение',
            'max_value' => 'Макс. значение',
            'diff_rate_one' => 'Разница ставок 1',
            'diff_rate_two' => 'Разница ставок 2',
            'created_at' => 'Создан',
            'is_limit' => 'Лимит'
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_ACCEPT => 'Прием заявок',
            self::STATUS_AWAIT => 'Ожидание результата',
            self::STATUS_END => 'Завершена',
        ];
    }

    /**
     * @return int
     */
    public function getStatusByTime()
    {
        if(date('Y-m-d H:i') > date('Y-m-d H:i',strtotime($this->datetime_accept_end))){
            if(date('Y-m-d H:i') > date('Y-m-d H:i',strtotime($this->datetime_end))){
                return self::STATUS_END;
            }
            return self::STATUS_AWAIT;
        }

        return self::STATUS_ACCEPT;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            $this->name .= $this->id;
            $this->save(false);
        }
    }

    /**
     * @return string
     */
    public function getUntilDateTimeAcceptEndText()
    {
        $now = time();
        $diff = strtotime($this->datetime_accept_end) - $now;

        $diff = round($diff / 60);
        if($diff <= 0){
            return "--:--";
        }
        return strlen(strval($diff)) == 2 ? "00:{$diff}" : "00:0{$diff}м";

        // if($diff > 0){
        //     $interval = date_diff(new \DateTime($this->datetime_accept_end), new \DateTime(date('Y-m-d H:i:s', $now)));

        //     if($interval->days > 0){
        //         return $interval->format("%a дней %H:%Iм");
        //     } else {
        //         return $interval->format("%H:%Iм");
        //     }
        // } else {
        //     return '00:00';
        // }
    }

    /**
     * @return string
     */
    public function getUntilDateTimeEndText()
    {
        $now = time();
        $diff = strtotime($this->datetime_end) - $now;

        $diff = round($diff / 60);
        if($diff <= 0){
            return "--:--";
        }
        return strlen(strval($diff)) == 2 ? "00:{$diff}" : "00:0{$diff}м";

        // if($diff > 0){
        //     $interval = date_diff(new \DateTime($this->datetime_end), new \DateTime(date('Y-m-d H:i:s', $now + 60)));

        //     if($interval->days > 0){
        //         return $interval->format("%a дней %H:%Iм");
        //     } else {
        //         return $interval->format("%H:%Iм");
        //     }
        // } else {
        //     return '00:00';
        // }
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getCurrentUserRate()
    {
        return RoomUser::find()->where(['user_id' => Yii::$app->user->getId(), 'room_id' => $this->id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomSetting()
    {
        return $this->hasOne(RoomSetting::className(), ['id' => 'room_setting_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomUsers()
    {
        return $this->hasMany(RoomUser::className(), ['room_id' => 'id']);
    }
}
