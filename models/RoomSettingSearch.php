<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RoomSetting;

/**
 * RoomSettingSearch represents the model behind the search form about `app\models\RoomSetting`.
 */
class RoomSettingSearch extends RoomSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'time_accept', 'time_await'], 'integer'],
            [['name', 'prefix'], 'safe'],
            [['min_value', 'max_value', 'commission', 'diff_rate_one', 'diff_rate_two'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RoomSetting::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'min_value' => $this->min_value,
            'max_value' => $this->max_value,
            'commission' => $this->commission,
            'diff_rate_one' => $this->diff_rate_one,
            'diff_rate_two' => $this->diff_rate_two,
            'time_accept' => $this->time_accept,
            'time_await' => $this->time_await,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'prefix', $this->prefix]);

        return $dataProvider;
    }
}
