<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Room;

/**
 * RoomSearch represents the model behind the search form about `app\models\Room`.
 */
class RoomSearch extends Room
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'room_setting_id', 'status'], 'integer'],
            [['name', 'datetime_accept_end', 'datetime_end', 'created_at'], 'safe'],
            [['service_commission', 'min_value', 'max_value', 'diff_rate_one', 'diff_rate_two'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Room::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
//                    'id' => SORT_DESC,
                    'name' => SORT_ASC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'room_setting_id' => $this->room_setting_id,
            'service_commission' => $this->service_commission,
            'status' => $this->status,
            'min_value' => $this->min_value,
            'max_value' => $this->max_value,
            'diff_rate_one' => $this->diff_rate_one,
            'diff_rate_two' => $this->diff_rate_two,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        if($this->datetime_end != null){
            $date = explode(' - ', $this->datetime_end);
            $query->andWhere(['between', 'datetime_accept_end', $date[0], $date[1]]);
        }

        if($this->datetime_accept_end != null){
            $date = explode(' - ', $this->datetime_accept_end);
            $query->andWhere(['between', 'datetime_accept_end', $date[0], $date[1]]);
        }

        if($this->created_at != null){
            $date = explode(' - ', $this->created_at);
            $query->andWhere(['between', 'datetime_accept_end', $date[0], $date[1]]);
        }


        return $dataProvider;
    }
}
