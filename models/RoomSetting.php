<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "room_setting".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $prefix Префикс
 * @property double $min_value Мин значение
 * @property double $max_value Макс значение
 * @property double $commission Коммисия
 * @property double $diff_rate_one Разница ставок 1
 * @property double $diff_rate_two Разница ставок 2
 * @property int $time_accept Время приема заявкок
 * @property int $time_await Время ожидания результата
 *
 * @property Room[] $rooms
 */
class RoomSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['min_value', 'max_value', 'commission', 'diff_rate_one', 'diff_rate_two'], 'number'],
            [['time_accept', 'time_await'], 'integer'],
            [['name', 'prefix'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'prefix' => 'Префикс',
            'min_value' => 'Мин значение',
            'max_value' => 'Макс значение',
            'commission' => 'Коммисия',
            'diff_rate_one' => 'Разница ставок 1',
            'diff_rate_two' => 'Разница ставок 2',
            'time_accept' => 'Время приема заявкок',
            'time_await' => 'Время ожидания результата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['room_setting_id' => 'id']);
    }
}
