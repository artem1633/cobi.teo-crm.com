<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $password_hash
 * @property string $password
 * @property integer $status
 * @property string $email
 * @property integer $is_deletable
 * @property string $phone
 * @property string $wallet_id
 * @property string $wallet_address
 *
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const LANG_RU = 'RU';

    const ROLE_ADMIN = 0;
    const ROLE_USER = 1;

    public $password;

    private $oldPasswordHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'login', 'wallet_id', 'wallet_address', 'is_deletable', 'password', 'password_hash', 'phone', 'lang', 'balance', 'reserve', 'role', 'created_at'],
            self::SCENARIO_EDIT => ['name', 'login', 'wallet_id', 'wallet_address', 'is_deletable', 'password', 'password_hash',  'phone', 'lang', 'balance', 'reserve', 'role', 'created_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
//            ['login', 'match', 'pattern' => '/^[a-z]+([-_]?[a-z0-9]+){0,2}$/i', 'message' => '{attribute} должен состоять только из латинских букв и цифр'],
//            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/', 'message' => '{attribute} не соответствует всем параметрам безопасности'],
            [['login'], 'unique'],
            [['is_deletable', 'role'], 'integer'],
            [['balance', 'reserve'], 'number'],
            [['login', 'password_hash', 'password', 'name', 'phone', 'lang', 'wallet_id', 'wallet_address'], 'string', 'max' => 255],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }

    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->password != null){
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'ФИО',
            'password_hash' => 'Password Hash',
            'password' => 'Пароль',
            'status' => 'Статус',
            'email' => 'Email',
            'is_deletable' => 'Удаляемый',
            'phone' => 'Телефон',
            'wallet_id' => 'Кошелек',
            'balance' => 'Баланс',
            'wallet_address' => 'Адрес кошелька'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function langLabels()
    {
        return [
            self::LANG_RU => 'Русский',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }


    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}
