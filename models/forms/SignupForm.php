<?php
namespace app\models\forms;

use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $userId;

    public $login;
    public $password;
    public $name;
    public $phone;
    public $lang;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],

            ['login', 'trim'],
            ['login', 'required'],
            ['login', 'email'],
            [['login', 'phone', 'lang'], 'string', 'max' => 255],
            ['login', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот email уже зарегистрирован',
                'when' => function($model, $attribute){
                    if($this->userId != null)
                    {
                        $userModel = User::findOne($this->userId);
                        return $this->{$attribute} !== $userModel->getOldAttribute($attribute);
                    }
                    return true;
                },
            ],

            ['password', 'string', 'min' => 6],
        ];
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios['update'] = ['password', 'email'];//Scenario Values Only Accepted
    //     return $scenarios;
    // }

    public function attributeLabels()
    {
        return [
            'login' => 'Email',
            'password' => 'Пароль',
            'name' => 'ФИО',
            'phone' => 'Номер телефона',
            'lang' => 'Язык'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        
        $user = new User();
        $user->login = $this->login;
        $user->phone = $this->phone;
        $user->lang = $this->lang;
        $user->role = User::ROLE_USER;
        $user->balance = 0;
        $user->reserve = 0;
        $user->setPassword($this->password);
        $user->save(false);

        return $user;
    }

    /**
     * update user.
     *
     * @param User $user
     * @return User|null the saved model or null if saving fails
     */
    public function update($user)
    {
        if (!$this->validate()) {
            return null;
        }

        $user->name = $this->name;
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->password = $this->password;

        return $user->update();
    }
}
