<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "room_user".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $room_id Комната
 * @property string $datetime_enter Дата и время входа
 * @property double $rate Ставка
 * @property int $rate_type На что ставка
 * @property int $won Победил/Не победил
 * @property double $get_payment Получил выплату
 * @property double $write_off Списали
 * @property string $datetime_end Дата и время завершения
 *
 * @property Room $room
 * @property User $user
 */
class RoomUser extends \yii\db\ActiveRecord
{
    const RATE_TYPE_UP = 0;
    const RATE_TYPE_DOWN = 1;
    const RATE_TYPE_AWAIT_UP = 3;
    const RATE_TYPE_AWAIT_DOWN = 4;
    const RATE_TYPE_AUTO = 5;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room_user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'datetime_enter',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rate', 'rate_type'], 'required'],
            [['user_id', 'room_id', 'rate_type', 'won'], 'integer'],
            [['datetime_enter', 'datetime_end'], 'safe'],
            [['rate', 'get_payment', 'write_off'], 'number'],
            ['rate', function(){
                $room = Room::findOne($this->room_id);

                if($room){
                    if($this->rate > $room->max_value){
                        $this->addError('rate', "Ставка не может быть больше максимального значения в {$room->max_value}");
                    } else if($this->rate < $room->min_value) {
                        $this->addError('rate', "Ставка не может быть меньше минимального значения в {$room->min_value}");
                    }
                }
            }],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'room_id' => 'Комната',
            'datetime_enter' => 'Дата и время подачи заявки',
            'rate' => 'Ставка',
            'rate_type' => 'На что ставка',
            'won' => 'Победил/Не победил',
            'get_payment' => 'Получил выплату',
            'write_off' => 'Списали',
            'datetime_end' => 'Дата и время завершения',
        ];
    }

    /**
     * @return array
     */
    public static function rateTypeLabels()
    {
        return [
            self::RATE_TYPE_UP => 'Покупка',
            self::RATE_TYPE_DOWN => 'Продажа',
            self::RATE_TYPE_AWAIT_UP => 'Ожидание покупки',
            self::RATE_TYPE_AWAIT_DOWN => 'Ожидание продажи',
            self::RATE_TYPE_AUTO => 'Авто'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
//        $all = RoomUser::find()->where(['room_id' => $this->room_id])->sum('rate');
//
//        $last = RoomUser::find()->where(['room_id' => $this->room_id, 'user_id' => Yii::$app->user->getId()])->one();
        $last = RoomUser::find()->where(['room_id' => $this->room_id, 'user_id' => $this->user_id])->one();
//
        if($last && $insert){
            \Yii::warning('refuse save, duplicate rate');
            return false;
        }


        \Yii::warning($this->rate_type, 'rate_type');
        if($this->rate_type != self::RATE_TYPE_AWAIT_UP && $this->rate_type != self::RATE_TYPE_AWAIT_DOWN && $this->rate_type != self::RATE_TYPE_AUTO){
            $up =  RoomUser::find()->where(['room_id' => $this->room_id, 'rate_type' => [RoomUser::RATE_TYPE_UP, RoomUser::RATE_TYPE_AWAIT_UP]])->sum('rate');
            $down =  RoomUser::find()->where(['room_id' => $this->room_id, 'rate_type' => [RoomUser::RATE_TYPE_DOWN, RoomUser::RATE_TYPE_AWAIT_DOWN]])->sum('rate');

            \Yii::warning("IN THE CONDITION");

            $small = 0;
            $big = 0;

            if($up > $down){
                $small = $down;
                $big = $up;
                $lim = self::RATE_TYPE_UP;
            } else if($up < $down){
                $small = $up;
                $big = $down;
                $lim = self::RATE_TYPE_DOWN;
            } else if($up == $down){
                $small = $up;
                $big = $down;
                $buy = "ALL";
                $sell = "ALL";
                $lim = null;
            }
            $needBalance = false;
            $diff = null;
            if($big > 0){
                $diff = round($small/$big, 2);

                if($this->room->is_limit == 1){
                    if($diff >= $this->room->diff_rate_two){
                        $this->room->is_limit = 0;
//                        $this->room->save(false);
                    }
                } else if($this->room->is_limit == 0){
                    if($diff <= $this->room->diff_rate_one){
                        $this->room->is_limit = 1;
//                        $this->room->save(false);
                    }
                }
                if($this->room->is_limit == 0){
                    $lim = null;
                }
//                if($diff < $this->room->diff_rate_one || $diff > $this->room->diff_rate_two){
//                    $needBalance = true;
//                } else {
//                    $lim = null;
//                }
            }

            \Yii::warning($lim, 'lim value');
            if($lim !== null){
                \Yii::warning($lim, 'LIM');
                if($this->rate_type == $lim){
                    \Yii::warning($lim, 'Rate types are equals');
                    $min = $this->room->min_value;
                    if($this->rate > $min){
                        Yii::warning("{$this->rate} > {$min}", '');
                        $rateDiff = $this->rate - $min;
                        $this->rate = $min;
                        $newRate = new self();
                        $newRate->attributes = $this->attributes;
                        $newRate->rate = $rateDiff;
                        if($this->rate_type == self::RATE_TYPE_UP){
                            $newRate->rate_type = self::RATE_TYPE_AWAIT_UP;
                        } else if($this->rate_type == self::RATE_TYPE_DOWN){
                            $newRate->rate_type = self::RATE_TYPE_AWAIT_DOWN;
                        }
                        $result = $newRate->save(false);
                        \Yii::warning($result, "Result new rate with type {$newRate->rate_type}");
                    }
                }
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
